type FibResponse = {
    index: number[],
    value: number[]
}

export default FibResponse;