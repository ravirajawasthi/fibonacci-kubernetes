package org.byteb.fibonacci.handler.db;

import lombok.extern.slf4j.Slf4j;
import org.byteb.fibonacci.handler.models.AllFibonacci;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
@Component
public class FibonacciDB {

    private final Connection postgresClient;

    FibonacciDB(Connection postgresClient) {
        this.postgresClient = postgresClient;
    }

    public Integer getFibFromDb(Integer index) {
        try (var pst = postgresClient.prepareStatement("SELECT val from fibonacci where index = ? LIMIT 1")) {
            pst.setInt(1, index);
            var resultSet = pst.executeQuery();
            if (resultSet.next())
                return resultSet.getInt(1);
            else {
                return null;
            }
        } catch (Exception ex) {
            log.error("Error occurred while trying to find fib({})", index);
            log.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    public void storeFibInDb(Integer index, Integer val) {
        try (var pst = postgresClient.prepareStatement("INSERT INTO fibonacci VALUES(?, ?)")) {
            pst.setInt(1, index);
            pst.setInt(2, val);
            var res = pst.executeUpdate();
            if (res < 0) {
                throw new SQLException("Unable to set value in db, Query execution failed for some reason!");
            }
        } catch (Exception exception) {
            log.error("Unable to populate db for fib({})", index);
            exception.printStackTrace();
            log.error(exception.getMessage());
        }
    }

    public AllFibonacci getAllFibs() {
        var allFibs = new AllFibonacci();
        try (var pst = postgresClient.prepareStatement("select index, val from fibonacci LIMIT 100")) {
            var res = pst.executeQuery();

            while (res.next()) {
                allFibs.getIndex().add(res.getInt(1));
                allFibs.getValues().add(res.getInt(2));
            }

        } catch (Exception exception) {
            log.error("Exception occurred while getting all fibonacci numbers from db");
            exception.printStackTrace();
            log.error(exception.getMessage());
        }
        return allFibs;

    }

    public void resetDb() {
        try(var pst = postgresClient.prepareStatement("DELETE FROM fibonacci")){
            var res = pst.executeUpdate();
            if (res < 0) {
                throw new SQLException("Unable to reset DB, Query execution failed for some reason!");
            }
        }
        catch (Exception exception){
            log.error("Exception occurred while resting db");
            exception.printStackTrace();
            log.error(exception.getMessage());
        }
    }
}
