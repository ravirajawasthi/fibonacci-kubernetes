package org.byteb.fibonacci.handler.service;

import lombok.extern.slf4j.Slf4j;
import org.byteb.fibonacci.handler.db.FibonacciDB;
import org.byteb.fibonacci.handler.models.AllFibonacci;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FibonacciService {


    private final String queueName;

    private final FibonacciDB fibonacciDB;
    private final RedisTemplate<Integer, Integer> redisTemplate;

    FibonacciService(FibonacciDB fibonacciDB,
                     @Qualifier("redisTemplateFibonacci") RedisTemplate<Integer, Integer> redisTemplate,
                     @Value("${redis.queue.name}") String queueName) {
        this.fibonacciDB = fibonacciDB;
        this.redisTemplate = redisTemplate;
        this.queueName = queueName;
    }

    public Integer fibRequest(Integer integer) throws InterruptedException {
        log.info("Calculating fib({})", integer);
        var fib = fibonacciDB.getFibFromDb(integer);
        if (fib == null) {
            log.info("fib({}) not in db, checking in redis", integer);
            fib = redisTemplate.opsForValue().get(integer);
            if (fib!=null){
                log.info("got fib({}) in redis, storing in DB", integer);
                fibonacciDB.storeFibInDb(integer, fib);
            }
        }
        if (fib == null) {
            log.info("fib({}) not in db and redis, pushing message to redis fibonacci queue", integer);
            redisTemplate.convertAndSend(queueName, integer);
            Thread.sleep(300L);
            fib = redisTemplate.opsForValue().get(integer);
            fibonacciDB.storeFibInDb(integer, fib);
        }
        return fib;
    }


    public AllFibonacci getAllFibonacci() {
        return fibonacciDB.getAllFibs();
    }

    public void restState() {
        log.info("Resting DB!");
        fibonacciDB.resetDb();
        log.info("DB rest complete!");
        log.info("flushing redis");
        redisTemplate.getConnectionFactory().getConnection().flushDb();
        log.info("flushing redis complete");
    }
}
