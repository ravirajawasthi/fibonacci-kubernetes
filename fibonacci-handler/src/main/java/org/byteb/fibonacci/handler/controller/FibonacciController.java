package org.byteb.fibonacci.handler.controller;

import org.byteb.fibonacci.handler.models.AllFibonacci;
import org.byteb.fibonacci.handler.service.FibonacciService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FibonacciController {
    FibonacciService fibonacciService;

    FibonacciController(FibonacciService fibonacciService) {
        this.fibonacciService = fibonacciService;
    }

    @GetMapping("/{fibonacciIndex}")
    @CrossOrigin(origins = "http://localhost:3000")
    Integer calculateFibonacciIndex(@PathVariable("fibonacciIndex") Integer fibIndex) throws InterruptedException {
        return fibonacciService.fibRequest(fibIndex);
    }

    @GetMapping("/allFibs")
    @CrossOrigin(origins = "http://localhost:3000")
    AllFibonacci getAllFibonacci(){
        return fibonacciService.getAllFibonacci();
    }

    @GetMapping("/reset")
    @CrossOrigin(origins = "http://localhost:3000")
    void resetState(){
        fibonacciService.restState();
    }

}
