package org.byteb.fibonacci.handler.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class AllFibonacci {
    @JsonProperty("index")
    private ArrayList<Integer> index;

    @JsonProperty("value")
    private ArrayList<Integer> values;

    public AllFibonacci(){
        index = new ArrayList<>();
        values = new ArrayList<>();
    }

    // Getter and Setters
    public ArrayList<Integer> getIndex() {
        return index;
    }

    public void setIndex(ArrayList<Integer> index) {
        this.index = index;
    }

    public ArrayList<Integer> getValues() {
        return values;
    }

    public void setValues(ArrayList<Integer> values) {
        this.values = values;
    }


}
