package org.byteb.fibonacci.handler.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;

@Configuration
public class DBConfig {

    @Bean("postgresDbClient")
    Connection getConnectionFactory(DataSource dataSource) throws SQLException {
        Resource initSchema = new ClassPathResource("db/init.sql");
        DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema, initSchema);
        DatabasePopulatorUtils.execute(databasePopulator, dataSource);
        return dataSource.getConnection();
    }

    @Bean("redisConnectionFactory")
    public RedisConnectionFactory getRedisConnectionFactory(@Value("${redis.host}") String redisHost,
                                                            @Value("${redis.port}") Integer redisPort) {
        LettuceClientConfiguration lettuceClientConfiguration =
                LettuceClientConfiguration.builder().commandTimeout(Duration.ofSeconds(2)).shutdownTimeout(Duration.ofSeconds(0)).build();

        return new LettuceConnectionFactory(new RedisStandaloneConfiguration(redisHost, redisPort),
                lettuceClientConfiguration);
    }

    @Bean("redisTemplateFibonacci")
    RedisTemplate<Integer, Integer> redisTemplate(@Qualifier("redisConnectionFactory") RedisConnectionFactory factory) {
        var rt = new RedisTemplate<Integer, Integer>();
        rt.setConnectionFactory(factory);
        rt.setKeySerializer(new Jackson2JsonRedisSerializer<>(Integer.class));
        rt.setValueSerializer(new Jackson2JsonRedisSerializer<>(Integer.class));
        rt.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(Integer.class));
        rt.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Integer.class));
        return rt;
    }

    @Bean
    DataSource createDataSource(@Value("${spring.datasource.url}") String url, @Value("${spring.datasource.username}") String username, @Value("${spring.datasource.password}") String password, @Value("${spring.datasource.driver-class-name}") String driverClass) {
        return DataSourceBuilder.create()
                .url(url)
                .username(username)
                .password(password)
                .driverClassName(driverClass)
                .build();
    }

}
