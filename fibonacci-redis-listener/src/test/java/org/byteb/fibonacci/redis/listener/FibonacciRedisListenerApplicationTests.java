//package org.byteb.fibonacci.redis.listener;
//
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.redis.core.ReactiveRedisOperations;
//import org.springframework.test.context.ActiveProfiles;
//import redis.embedded.RedisServer;
//
//import java.util.concurrent.TimeUnit;
//
//import static org.awaitility.Awaitility.await;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@SpringBootTest
//@ActiveProfiles("test")
//class FibonacciRedisListenerApplicationTests {
//
//    @Value("${redis.queue.name}")
//    String queueName;
//
//    static RedisServer redisServer = new RedisServer();
//
//    @Autowired
//    ReactiveRedisOperations<Integer, Integer> redisTemplate;
//
//    @BeforeAll
//    static void setUp() {
//        redisServer.start();
//    }
//
//    @AfterAll
//    static void tearDown() {
//        redisServer.stop();
//    }
//
//
//    @Test
//    void pushingMessage() {
//        for (int i = 10 ; i > 0 ; i--) {
//            redisTemplate.convertAndSend(queueName, i).block();
//        }
//        //Required so messages pushed to redis can be read.
//        await().atMost(5, TimeUnit.SECONDS).until(() -> {
//            var fib10 = redisTemplate.opsForValue().get(10).blockOptional();
//            assertTrue(fib10.isPresent());
//            assertEquals(55, fib10.get());
//
//            var fib5 = redisTemplate.opsForValue().get(5).blockOptional();
//            assertTrue(fib5.isPresent());
//            assertEquals(5, fib5.get());
//            return true;
//        });
//
//
//    }
//
//}
