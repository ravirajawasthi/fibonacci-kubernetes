package org.byteb.fibonacci.redis.listener.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import java.time.Duration;

@Configuration
public class RedisConfig {

    @Bean("reactiveRedisConnectionFactory")
    public ReactiveRedisConnectionFactory getReactiveRedisConnectionFactory(@Value("${redis.host}") String redisHost,
                                                                            @Value("${redis.port}") Integer redisPort) {
        LettuceClientConfiguration lettuceClientConfiguration =
                LettuceClientConfiguration.builder().commandTimeout(Duration.ofSeconds(2)).shutdownTimeout(Duration.ofSeconds(0)).build();

        return new LettuceConnectionFactory(new RedisStandaloneConfiguration(redisHost, redisPort),
                                            lettuceClientConfiguration);
    }

    @Bean("reactiveRedisTemplateFibonacci")
    ReactiveRedisOperations<Integer, Integer> reactiveRedisTemplate(@Qualifier("reactiveRedisConnectionFactory") ReactiveRedisConnectionFactory factory) {
        return new ReactiveRedisTemplate<>(factory,
                                           RedisSerializationContext.<Integer, Integer>newSerializationContext()
                                                   .key(new Jackson2JsonRedisSerializer<>(Integer.class))
                                                   .value(new Jackson2JsonRedisSerializer<>(Integer.class))
                                                   .hashKey(new Jackson2JsonRedisSerializer<>(Integer.class))
                                                   .hashValue(new Jackson2JsonRedisSerializer<>(Integer.class))
                                                   .build());
    }


}
