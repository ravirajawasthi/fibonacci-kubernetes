package org.byteb.fibonacci.redis.listener.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveValueOperations;
import org.springframework.stereotype.Component;

@Component
public class FibonacciCalculator {

    Logger log = LoggerFactory.getLogger(FibonacciCalculator.class);

    ReactiveValueOperations<Integer, Integer> reactiveRedisTemplateFibonacci;

    FibonacciCalculator(@Qualifier("reactiveRedisTemplateFibonacci") ReactiveRedisOperations<Integer, Integer> reactiveRedisTemplateFibonacci) {
        this.reactiveRedisTemplateFibonacci = reactiveRedisTemplateFibonacci.opsForValue();
    }

    public void calculateFibonacci(Integer index) {
        log.info("calculateFibonacci called for index = {}", index);
        var ans = reactiveRedisTemplateFibonacci.get(index).blockOptional();
        if (ans.isPresent()) {
            return;
        }
        var fibonacci = calculateFibonacciRecursion(index);
        reactiveRedisTemplateFibonacci.set(index, fibonacci).block();
    }

    private Integer calculateFibonacciRecursion(Integer integer) {
        if (integer < 2) {

            log.info("fib({}) = {}", integer, integer);
            return integer;

        } else {

            var prev = reactiveRedisTemplateFibonacci.get(integer - 1).blockOptional();
            var prevPrev = reactiveRedisTemplateFibonacci.get(integer - 2).blockOptional();

            int fibonacciPrev;
            int fibonacciPrevPrev;

            if (prev.isEmpty()) {
                fibonacciPrev = calculateFibonacciRecursion(integer - 1);
                reactiveRedisTemplateFibonacci.set(( integer - 1 ), ( fibonacciPrev )).block();
            } else {
                fibonacciPrev = prev.get();
            }

            if (prevPrev.isEmpty()) {
                fibonacciPrevPrev = calculateFibonacciRecursion(integer - 2);
                reactiveRedisTemplateFibonacci.set(( integer - 2), ( fibonacciPrevPrev )).block();
            } else {
                fibonacciPrevPrev = prevPrev.get();
            }

            int ans = fibonacciPrev + fibonacciPrevPrev;
            log.info("fib({}) = {}", integer, ans);

            return ans;
        }
    }


}
