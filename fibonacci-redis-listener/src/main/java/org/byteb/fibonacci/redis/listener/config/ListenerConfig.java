package org.byteb.fibonacci.redis.listener.config;

import jakarta.annotation.PostConstruct;
import org.byteb.fibonacci.redis.listener.listener.FibonacciCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveSubscription;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.util.Date;


@Configuration
public class ListenerConfig {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    Logger log = LoggerFactory.getLogger(ListenerConfig.class);
    private final String queueName;
    private final ReactiveRedisOperations<Integer, Integer> reactiveRedisOperations;

    private final FibonacciCalculator fibonacciCalculator;

    ListenerConfig(@Qualifier("reactiveRedisTemplateFibonacci") ReactiveRedisOperations<Integer, Integer> reactiveRedisOperations,
                   FibonacciCalculator fibonacciCalculator,
                   @Value("${redis.queue.name}") String queueName) {
        this.reactiveRedisOperations = reactiveRedisOperations;
        this.fibonacciCalculator = fibonacciCalculator;
        this.queueName = queueName;
    }

    @PostConstruct
    private void init() {
        this.reactiveRedisOperations
                .listenTo(ChannelTopic.of(queueName))
                .map(ReactiveSubscription.Message::getMessage)
                .subscribe(fibonacciCalculator::calculateFibonacci);
    }

    //For keeping the application Alive
    @Scheduled(fixedRate = 1000 * 60) // 1 minute
    public void reportCurrentTime() {
        var time = dateFormat.format(new Date());
        log.info("Keepalive at time {}", time);
    }


}
