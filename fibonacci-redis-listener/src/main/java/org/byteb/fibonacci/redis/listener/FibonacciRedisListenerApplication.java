package org.byteb.fibonacci.redis.listener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class FibonacciRedisListenerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FibonacciRedisListenerApplication.class, args);
    }

}
